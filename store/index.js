// 页面路径：store/index.js 
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);//vue的插件机制

//引入子模块
import WEBCONFIG from './modules/webconfig'
import USER from './modules/user'
import GLOBALSTATUS from './modules/globalstatus'
//Vuex.Store 构造器选项
const store = new Vuex.Store({
    modules:{
        WEBCONFIG,
        USER,
        GLOBALSTATUS
    }
})
export default store