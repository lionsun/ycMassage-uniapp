import { useStorage } from '@/utils'
const userStorageKey = "USERINFO";
export default {
	state: {
		USERINFO: {},
		STATICTIS: {
			service: '--',
			technician: '--',
			unread: '--',
			system: '--',
			technician: '--',
			order_wait_start: '--',
			money: '--',
			integral: '--',
			coupon_total: '--',
			coupon_used: '--',
			coupon_unuse: '--',
			coupon_unreceive: '--',
		}
	},
	getters: {
		hasLogin: state => {
			return !!state.USERINFO && !!state.USERINFO.token;
		},
		getToken: state => {
			return state.USERINFO.token;
		},
		hasUserPermission: state => permission => {
			return state.USERINFO && state.USERINFO.permissions && state.USERINFO.permissions.includes(permission);
		}
	},
	mutations: {
		setUserInfo(state, userInfo) {
			state.USERINFO = userInfo;
		},
		setStatictis(state, statictis) {
			state.STATICTIS = statictis;
		}
	},
	actions: {
		initUserInfo({ commit }) {
			const storage = useStorage();
			const res = storage.get(userStorageKey);
			if (res) {
				commit("setUserInfo", res);
			}
		},
		setUserInfo({ commit }, userInfo) {
			const storage = useStorage();
			return new Promise((resolve, reject) => {
				storage.set(userStorageKey, userInfo).then(() => {
					commit("setUserInfo", userInfo);
					resolve();
				}).catch(() => {
					reject();
				})
			});
		},
		setStatictis({ commit }, statictis) {
			commit("setStatictis", statictis);
		},
		clearUserInfo({ commit }) {
			const storage = useStorage();
			commit("setUserInfo", {});
			return storage.remove(userStorageKey);
		}
	}
}
