import { useStorage } from "@/utils/storage";
const StatusStorageKey = 'GLOBALSTATUS'
export default {
	state: {
        location:null,
        city:{
            title:'选择城市',
        },
        tabbarStyle:'system',
		tabbar:[{
            page:'SWITCH',
            title: '首页',
            url: '/pages/index/index',
            image: '/static/tabbar/home.png',
            image_2: '/static/tabbar/home_active.png'
        }, {
            page:'SWITCH',
            title: '技师',
            url: '/pages/technician/index',
            image: '/static/tabbar/technician.png',
            image_2: '/static/tabbar/technician_active.png'
        }, {
            page:'SWITCH',
            url: "/pages/mch/index",
            title: "商户",
            image: "/static/tabbar/mch.png",
            image_2: "/static/tabbar/mch_active.png"
        },{
            page:'SWITCH',
            title: '订单',
            url: '/pages/order/list',
            image: '/static/tabbar/order.png',
            image_2: '/static/tabbar/order_active.png'
        }, {
            page:'SWITCH',
            title: '我的',
            url: '/pages/user/index',
            image: '/static/tabbar/user.png',
            image_2: '/static/tabbar/user_active.png'
        }]
	},
	getters: {
	},
	mutations: {
        setData(state, data) {
            for (let key in data) {
                state[key] = data[key];
            }
        },
        removeTabbarMch(state){
            state.tabbar = state.tabbar.filter(item=>!item.url.includes('pages/mch/index'));
        },
        setTabbar(state,tabbar){
            state.tabbarStyle='custom';
            state.tabbar = tabbar;
        }
	},
	actions: {
        initGlobalstatus(context){
            const storage = useStorage();
            const res=storage.get(StatusStorageKey);
            if(res){
                context.commit('setData',res);
            }
        },
        clearGlobalstatus(context){
			const storage = useStorage();
            storage.remove(StatusStorageKey);
        },
        removeTabbarMch({commit}){
            commit('removeTabbarMch');
        },
        setTabbar(context,tabbar){
            context.commit('setTabbar',tabbar);
			const storage = useStorage();
			return new Promise((resolve,reject)=>{
				storage.set(StatusStorageKey,context.state,3600*24).then(()=>{
					resolve();
				}).catch(()=>{
					reject();
				});
			});
        },
        setCity(context,city){
            context.commit('setData',{city});
            const storage = useStorage();
            return new Promise((resolve,reject)=>{
                storage.set(StatusStorageKey,context.state,3600*24).then(()=>{
                    resolve();
                }).catch(()=>{
                    reject();
                });
            });
        },
        setLocation(context,location){
            context.commit('setData',{location});
            const storage = useStorage();
            return new Promise((resolve,reject)=>{
                storage.set(StatusStorageKey,context.state,3600*24).then(()=>{
                    resolve();
                }).catch(()=>{
                    reject();
                });
            });
        }
	}
}
