import {useStorage} from '@/utils'
const WebConfigStorageKey = 'WEBCONFIG'
export default {
	state: {
	},
	getters: {
		hasPermission:state=>permission=>{
			return !!state?.[permission];
		},
	},
	mutations: {
		setWebConfig(state,webconfig){
			for(let key in webconfig){
				state[key]=webconfig[key];
			}
		}
	},
	actions: { 
		initWebConfig({commit}){
			const storage = useStorage();
			const res=storage.get(WebConfigStorageKey);
			if(res){
				commit("setWebConfig",res);
			}
		},
		setWebConfig({commit},webconfig){
			const storage = useStorage();
			return new Promise((resolve,reject)=>{
				storage.set(WebConfigStorageKey,webconfig).then(()=>{
					commit("setWebConfig",webconfig);
					resolve();
				}).catch(()=>{
					reject();
				});
			});
		}
	}
}
