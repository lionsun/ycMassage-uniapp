/**
 * 节流函数会确保在指定的时间间隔内，函数被调用的次数不超过设定的阈值。
 * @param func 节流函数
 * @param wait 时间（毫秒）
 * @returns function
 */
export function throttle(func, wait) {
    let previous = 0;
    return function() {
      let now = Date.now();
      let context = this;
      let args = arguments;
      if (now - previous > wait) {
        func.apply(context, args);
        previous = now;
      }
    }
}
/**
 * 防抖函数会延迟函数的执行，直到一定的静默期过去后，才真正执行该函数。
 * @param func 防抖函数
 * @param wait 时间（毫秒）
 * @returns function
 */
export function debounce(func, wait) {
    let timeout;
    return function() {
      let context = this;
      let args = arguments;
      if (timeout) clearTimeout(timeout);
      timeout = setTimeout(() => {
        func.apply(context, args);
      }, wait);
    }
}
/**
 * 格式化数字
 * @param num 数字
 * @returns string
 */
export function formatNumber(num) {
  if(typeof num !== 'number'){
    if(Number.isNaN(Number(num))){
      return num;
    }
    num=Number(num);
  }
  if (num >= 100000000) {
    return (num / 100000000).toFixed(1) + '亿';
  }else if (num >= 10000) {
    return (num / 10000).toFixed(1) + '万';
  }
  return num.toFixed(2);
}
/**
 * 拨打电话
 * @param phoneNumber 电话号码
 */
export function makePhoneCall(phoneNumber) {
  uni.makePhoneCall({
    phoneNumber: phoneNumber
  });
}