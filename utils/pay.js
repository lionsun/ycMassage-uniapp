import {$http} from './http';
import * as $api from './api';
export const usePay=(OrderInfo)=>{
    let OrderStateNum = 0;
    const onBridgeReady = (resolve,reject) => {
        WeixinJSBridge.invoke('getBrandWCPayRequest', OrderInfo.pay, (res) => {
            if (res.err_msg == "get_brand_wcpay_request:ok") {
                OrderStateNum = 1;
                getOrderState(resolve,reject);
            }else{
                reject();
            }
        });
    }
    const getOrderState = (resolve,reject) => {
        $api.Orders.getOrderState(OrderInfo.order.trade).then((res) => {
            if (res.code === $http.ResponseCode.SUCCESS) {
                resolve();
            } else if (res.code === $http.ResponseCode.PAY_NOTPAY) {
                OrderStateNum++;
                if (OrderStateNum > 10) {
                    reject({msg:'支付失败'});
                    return;
                }
                setTimeout(()=>{
                    getOrderState(resolve,reject);
                }, 1000)
            }
        }).catch((err) => {
            reject(err);
        })
    }
    return new Promise((resolve,reject)=>{
        switch (OrderInfo.pay_type) {
            case 'wxpay':
                // #ifdef H5
                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady', ()=>{
                            onBridgeReady(resolve,reject)
                        }, false);
                    } else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', ()=>{
                            onBridgeReady(resolve,reject)
                        });
                        document.attachEvent('onWeixinJSBridgeReady', ()=>{
                            onBridgeReady(resolve,reject)
                        });
                    }
                } else {
                    onBridgeReady(resolve,reject);
                }
                // #endif
                // #ifdef MP-WEIXIN
                uni.requestPayment({
                    provider: 'wxpay',
                    timeStamp: OrderInfo.pay.timeStamp,
                    nonceStr: OrderInfo.pay.nonceStr,
                    package: OrderInfo.pay.package,
                    signType: OrderInfo.pay.signType,
                    paySign: OrderInfo.pay.paySign,
                    orderInfo: OrderInfo.orderInfo,
                    success: (res) => {
                        OrderStateNum = 1;
                        getOrderState(resolve,reject);
                    },
                    fail: (err) => {
                        reject({msg:'支付失败'});
                    }
                });
                // #endif
                break;
            case 'alipay':
            case 'epay':
                // #ifdef H5
                window.location.href = OrderInfo.pay_url;
                // #endif
                break;
        }
    })
}