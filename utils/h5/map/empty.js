export class EmptyMap
{
    constructor(callback){
    }
    isReady(){
        return false;
    }
    createMap(id){
        return new Promise((resolve,reject)=>{
            return reject('map is not ready');
        });
    }
    createMarker(map,lnglat){
        return new Promise((resolve,reject)=>{
            return reject('map is not ready');
        });
    }
    getCurrentPosition(){
        return new Promise((resolve,reject)=>{
            return reject('map is not ready');
        });
    }
    getPosition(address){
        return new Promise((resolve,reject)=>{
            return reject('map is not ready');
        });
    }
    getAddress(lnglat){
        return new Promise((resolve,reject)=>{
            return reject('map is not ready');
        });
    }
}