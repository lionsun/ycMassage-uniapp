import AMapLoader from '@amap/amap-jsapi-loader';
export class Amap
{
    constructor(amap_key,js_code,callback){
        if(amap_key&&js_code){
            globalThis._AMapSecurityConfig = { securityJsCode: js_code };
            AMapLoader.load({
                key: amap_key, // 申请好的Web端开发者Key，首次调用 load 时必填
                version: "2.0", // 指定要加载的 JSAPI 的版本，缺省时默认为 1.4.15
                plugins: [], // 需要使用的的插件列表，如比例尺'AMap.Scale'等
            })
                .then((AMap) => {
                    this.AMap=AMap;
                    AMap.plugin('AMap.Geocoder', ()=>{
                        this.geocoder = new AMap.Geocoder()
                    })
                    AMap.plugin("AMap.Geolocation", () => {
                        this.geolocation = new AMap.Geolocation({
                            enableHighAccuracy: true, // 是否使用高精度定位，默认：true
                            timeout: 10000, // 设置定位超时时间，默认：无穷大
                            offset: [10, 20],  // 定位按钮的停靠位置的偏移量
                            zoomToAccuracy: true,  //  定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
                            position: 'RB', //  定位按钮的排放位置,  RB表示右下
                            showMarker: false, //  定位成功后在定位到的位置显示点标记，默认：true
                            showCircle: false, //  定位成功后用圆圈表示定位精度范围，默认：true
                        })
                    });
                    typeof callback==='function'&&callback();
                })
                .catch((e) => {
                    console.log(e);
                });
        }
    }
    isReady(){
        return !!this.AMap;
    }
    createMap(id){
        return new Promise((resolve,reject)=>{
            if(!this.AMap){
                return reject('AMap is not ready');
            }
            const map = new this.AMap.Map(id, {
                // 设置地图容器id
                viewMode: "2D",
                zoom: 15
            });
            map.addControl(this.geolocation);
            resolve(map);
        });
    }
    createMarker(map,lnglat){
        return new Promise((resolve,reject)=>{
            if(!this.AMap){
                return reject('AMap is not ready');
            }
            this.AMap.plugin('AMap.MoveAnimation',()=>{
                const center=map.getCenter();
                const labelMarker = new this.AMap.LabelMarker({
                    position: [center.lng, center.lat],
                    opacity: 1,
                    zIndex: 2,
                    icon: {
                        image: 'static/image/order/location.png',
                        anchor: 'bottom-center',
                        size: [30, 29],
                        clipOrigin: [0, 0],
                        clipSize: [87, 86]
                    }
                });
                const labelsLayer = new this.AMap.LabelsLayer({
                    collision: true,
                });
                labelsLayer.add(labelMarker);
                map.add(labelsLayer);
                if(lnglat){
                    labelMarker.moveTo(lnglat);
                    map.setCenter(lnglat);
                }
                resolve(labelMarker);
            });
        });
    }
    getCurrentPosition(){
        return new Promise((resolve,reject)=>{
            if(!this.geolocation){
                return reject('geolocation is not ready');
            }
            this.geolocation.getCurrentPosition((status, result) => {
                if (status == 'complete') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
        });
    }
    getPosition(address){
        return new Promise((resolve,reject)=>{
            if(!this.geocoder){
                return reject('geocoder is not ready');
            }
            this.geocoder.getLocation(address, (status, result) => {
                if (status == 'complete'&&result.info === 'OK') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
        });
    }
    getAddress(lnglat){
        return new Promise((resolve,reject)=>{
            if(!this.geocoder){
                return reject('geocoder is not ready');
            }
            this.geocoder.getAddress(lnglat, (status, result) => {
                if (status == 'complete'&&result.info === 'OK') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
        });
    }
}