/**
 * 替换地址栏#参数不跳转
 * @param key 参数名
 * @param value 参数值
 * @returns void
 * @example replaceUrlParam('key','value')
 */
export function replaceUrlParam(key, value) {
    const url = new URL(globalThis.location.href);
	const queryArr=url.hash.split('?');
	const hashParams = new URLSearchParams(queryArr[1]??''); // 获取hash中的参数
    // 设置新的参数值
    hashParams.set(key, value);
    // 更新hash
    url.hash = queryArr[0]+'?'+hashParams.toString();
    globalThis.history.replaceState({}, '', url.href);
}
/**
 * 批量替换地址栏#参数不跳转
 * @param params 参数对象
 * @returns void
 * @example replaceUrlParams({key1:'value1',key2:'value2'})
 */
export function replaceUrlParams(params) {
	const url = new URL(globalThis.location.href);
	const queryArr=url.hash.split('?');
	const hashParams = new URLSearchParams(queryArr[1]??''); // 获取hash中的参数
	// 设置新的参数值
	for(let x in params){
		hashParams.set(x, params[x]);
	}
	// 更新hash
	url.hash = queryArr[0]+'?'+hashParams.toString();
	globalThis.history.replaceState({}, '', url.href);
}