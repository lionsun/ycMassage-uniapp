import store from '@/store';
import {EmptyMap} from './map/empty';
import {Amap} from './map/amap';
export const useMap=(callback)=>{
    switch(store.state.WEBCONFIG.map_channels)
    {
        case 'amap':
            return new Amap(store.state.WEBCONFIG.amap_web_key,store.state.WEBCONFIG.amap_js_code,callback);
        default:
            return new EmptyMap(callback);
    }
}