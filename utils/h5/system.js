import { useStorage } from "@/utils/storage";

export default{
	platform:()=>{
		return 'h5';
	},
	isDev:()=>{
		return process.env.NODE_ENV !== 'production';
	},
	isProd:()=>{
		return process.env.NODE_ENV === 'production';
	},
	info:()=>{
		return uni.getSystemInfoSync();
	},
	isAndroid:()=>{
		return uni.getSystemInfoSync().platform==='android';
	},
	isIOS:()=>{
		return uni.getSystemInfoSync().platform==='ios';
	},
	isWechat:()=>{
		return uni.getSystemInfoSync().ua.includes('MicroMessenger');
	},
	appid:()=>{
		return useStorage().get('APPID');
	},
    host:()=>{
		let baseURL = globalThis.location.origin
        return baseURL;
    },
	name:()=>{
		return '上门按摩';
	}
}