import store from '../store';
// #ifdef H5
import system from './h5/system';
// #endif
// #ifdef MP-WEIXIN
import system from './mp-weixin/system';
// #endif
const buildOptions = (options,hasLogin) => {
	// #ifdef H5
	const appid = system.appid();
	if (options.url.indexOf('?') > -1) {
		options.url += '&';
	} else {
		options.url += '?';
	}
	options.url += `appid=${appid}`;
	// #endif
	if (hasLogin) {
		if(!store.getters.hasLogin){
			options.url = '/pages/login/login';
			uni.navigateTo(options);
			return false;
		}
		if (options.url.startsWith('/technician/pages/') && !options.url.includes('/technician/pages/user/apply')) {
			if (!store.getters.hasUserPermission('technician')) {
				options.url = '/technician/pages/user/apply';
				uni.navigateTo(options);
				return false;
			}
		}
		if (options.url.startsWith('/agent/pages/') && !options.url.includes('/agent/pages/user/apply')) {
			if (!store.getters.hasUserPermission('agent')) {
				options.url = '/agent/pages/user/apply';
				uni.navigateTo(options);
				return false;
			}
		}
	}
	return options;
}
export const $page = {
	open: (params, hasLogin) => {
		let options = {
			url: ''
		};
		if (typeof params === 'string') {
			options.url = params;
		} else {
			options = Object.assign(options, params);
		}
		options = buildOptions(options,hasLogin);
		if (options) {
			uni.navigateTo(options);
		}
	},
	redirect: (params, hasLogin) => {
		let options = {
			url: ''
		};
		if (typeof params === 'string') {
			options.url = params;
		} else {
			options = Object.assign(options, params);
		}
		options = buildOptions(options,hasLogin);
		if (options) {
			uni.redirectTo(options)
		}
	},
	re: (params, hasLogin) => {
		let options = {
			url: ''
		};
		if (typeof params === 'string') {
			options.url = params;
		} else {
			options = Object.assign(options, params);
		}
		options = buildOptions(options,hasLogin);
		if (options) {
			uni.reLaunch(options)
		}
	},
	switch: (params) => {
		let options = {
			url: ''
		};
		if (typeof params === 'string') {
			options.url = params;
		} else {
			options = Object.assign(options, params);
		}
		options = buildOptions(options);
		if (options) {
			uni.switchTab(options);
		}
	},
	back: (delta) => {
		if (getCurrentPages().length < 2) {
			return uni.reLaunch({
				url: '/pages/index/index'
			});
		}
		uni.navigateBack({
			delta: delta || 1
		})
	},
	ads: (item) => {
		switch (item.page) {
			case "INNER":
				$page.open(item.url);
				break;
			case "ARTICLE":
				$page.open(`/pages/article/content?id=${item.alias_id}`);
				break;
			case "SWITCH":
				$page.switch(item.url);
				break;
			case "CUSTOM":
				// #ifdef H5
				globalThis.open(item.url);
				// #endif
				break;
		}
	},
	title: (title, subtitle) => {
		const WEBCONFIG = store.state.WEBCONFIG;
		const fastTitle = title || WEBCONFIG?.web_name || system.name();
		const lastTitle = subtitle !== undefined ? subtitle : '';
		uni.setNavigationBarTitle({
			title: fastTitle + lastTitle
		})
	},
}
export const login = (_this, data, xLoginRef) => {
	xLoginRef.show(data);
}
export const wxLogin = (_this, data) => {
	if (!system.isWechat()) {
		return _this.$utils.$message.error('请先安装微信客户端');
	}
	if (_this.$store.state.WEBCONFIG.wx_state) {
		_this.$utils.$api.Login.wechatLogin(data)
			.then((res) => {
				globalThis.location.href = res.data;
			}).catch((err) => {
				_this.$utils.$message.error(err.msg);
			})
	} else {
		_this.$utils.$message.error('请先配置微信公众号');
	}
}