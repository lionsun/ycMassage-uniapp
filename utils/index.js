export * from './storage';
export * from './http';
export * from './page';
export * from './message';
export * from './auth';
export * as $api from './api';
export * from './pay';
export * from './functions';
// #ifdef H5
export * from './h5/functions';
import system from './h5/system';
export const $system=system;
export * from './h5/map';
// #endif
// #ifdef MP-WEIXIN
import system from './mp-weixin/system';
export const $system=system;
export * from './mp-weixin/map';
// #endif