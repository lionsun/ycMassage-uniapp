import {siteinfo} from "@/siteinfo";
export default{
	platform:()=>{
		return 'mp-weixin';
	},
	isDev:()=>{
		return process.env.NODE_ENV !== 'production';
	},
	isProd:()=>{
		return process.env.NODE_ENV === 'production';
	},
	info:()=>{
		return uni.getSystemInfoSync();
	},
	isAndroid:()=>{
		return uni.getSystemInfoSync().platform==='android';
	},
	isIOS:()=>{
		console.log(uni.getSystemInfoSync());
		return uni.getSystemInfoSync().platform==='ios';
	},
	isWechat:()=>{
		return false;
	},
    appid:()=>{
        return siteinfo.app_id;
    },
    host:()=>{
        return siteinfo.siteroot;
    },
	name:()=>{
		return siteinfo.name;
	}
}