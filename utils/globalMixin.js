import { login } from '@/utils';
export default {
  methods: {
	login(options){
		login(this,options,this.$refs.xLoginRef)
	},
	formatDate(timestamp,formatType){
		const date = new Date(timestamp * 1000);
		const year = date.getFullYear();
		const month = ("0" + (date.getMonth() + 1)).slice(-2);
		const day = ("0" + date.getDate()).slice(-2);
			
		let formattedDate = '';
			
		switch (formatType) {
			case 'YYYY年MM月DD日':
			  formattedDate = `${year}年${month}月${day}日`;
			  break;
			case 'MM/DD/YYYY':
			  formattedDate = `${month}/${day}/${year}`;
			  break;
			default:
			  formattedDate = `${year}-${month}-${day}`;
		}
			
		return formattedDate;		
	},
	getDayName(d) {
		const now = d.getTime();
		const start = new Date();
		start.setHours(0, 0, 0, 0);
		const end = new Date();
		end.setHours(23, 59, 59, 999);
		switch (true) {
			case now >= start.getTime() && now <= end.getTime():
				return '今天';
			case now >= start.getTime() + 86400000 && now <= end.getTime() + 86400000:
				return '明天';
			case now >= start.getTime() + 172800000 && now <= end.getTime() + 172800000:
				return '后天';
			default:
				return (d.getMonth() + 1) + '月' + d.getDate() + '日';
		}
	},
	getTimeText(t){
		let text='';
		let h=0,hText='';
		if(t>3600){
			h=Math.floor(t/3600);
			hText=`${h}小时`;
			t=t%3600
		}
		const m=Math.floor(t / 60).toString().padStart(2, '0');
		const s=Math.ceil(t % 60).toString().padStart(2, '0');
		text=`${hText}${m}分${s}秒`
		return text;
	}
  },
}
