const storage = {
	setItem: (key, data) => {
		return uni.setStorage({
			key,
			data
		});
	},
	getItem: (key) => {
		return uni.getStorageSync(key)
	},
	removeItem: (key) => {
		return uni.removeStorage({
			key
		});
	}
};
export default {
	storagePrefix: 'MASSAGE',
	storage,
};