import { $http } from './http';
export const Ads={
    ads:(position)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Ads/list', {
                data: {
                    position
                }
            }).then(res=>{
                if (res.code === $http.ResponseCode.SUCCESS) {
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        });
    }
};
export const Config={
    webconfig:(_this)=>{
        $http.get('Config/index',{
            data:{
                platform:_this.$utils.$system.platform()
            }
        }).then((res)=>{
            if(res.code===$http.ResponseCode.SUCCESS){
                if(res.data.single_mch){
                    _this.$store.dispatch('removeTabbarMch');
                }
                _this.$store.dispatch('setWebConfig',res.data);
            }
        }).catch(()=>{
        })
    }
}
export const User={
    userinfo:(_this,silent)=>{
        $http.get('User/info').then((res)=>{
            if(res.code===$http.ResponseCode.SUCCESS){
                _this.$store.dispatch('setUserInfo',res.data).catch(()=>{});
            }else if(res.code===$http.ResponseCode.NEED_LOGIN){
                _this.$store.dispatch('clearUserInfo').catch(()=>{});
            }else{
                if(!silent){
                    _this.$utils.$message.error(res.msg);
                }
            }
        }).catch(()=>{
            if(!silent){
                _this.$utils.$message.error('获取用户信息失败');
            }
        })
    },
    save(data){
        return new Promise((resolve,reject)=>{
            $http.post('User/save',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    mobile(data){
        return new Promise((resolve,reject)=>{
            $http.post('User/mobile',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    password(data){
        return new Promise((resolve,reject)=>{
            $http.post('User/password',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    statictis(_this){
        return new Promise((resolve,reject)=>{
            $http.get('User/statictis').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    _this.$store.dispatch('setStatictis',res.data);
                    resolve();
                }else{
                    reject();
                }
            }).catch(()=>{
                reject();
            })
        })
    },
    unBindWechat(){
        return new Promise((resolve,reject)=>{
            $http.get('User/unBindWechat').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    bindWechat(data){
        return new Promise((resolve,reject)=>{
            $http.post('User/bindWechat',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    newUserGift(){
        return new Promise((resolve,reject)=>{
            $http.get('User/newUserGift').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Regions={
    getRegions:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('Regions/cache').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Address={
    saveAddress:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Address/saveAddress',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getAddressDetails:(id)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Address/getAddressDetails',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getAddress:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('Address/getAddress').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
};
export const Technician={
    list:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Technician/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    details:(technician_id)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Technician/details',{
                data:{
                    technician_id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getServiceTime:(technician_id)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Technician/getServiceTime',{
                data:{
                    technician_id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
                
            })
        })
    },
    info:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('Technician/info').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    deleteTechnicianImage:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Technician/deleteTechnicianImage',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    apply:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Technician/apply',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getDistance:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Technician/getDistance',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    statistics(query){
        return new Promise((resolve,reject)=>{
            $http.get('Technician/statistics',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    recommendExchange(data){
        return new Promise((resolve,reject)=>{
            $http.post('Technician/recommendExchange',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
                
            })
        })
    
    },
    shop(){
        return new Promise((resolve,reject)=>{
            $http.get('Technician/shop').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
                
            })
        })
    
    }
}
export const TechnicianService={
    selection:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('TechnicianService/selection',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
                
            })
        })
    },
    service:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('TechnicianService/service',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    joinService:(service_id)=>{
        return new Promise((resolve,reject)=>{
            $http.post('TechnicianService/joinService',{ data:{service_id} }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    updateState:(uts_id)=>{
        return new Promise((resolve,reject)=>{
            $http.post('TechnicianService/updateState',{ data:{uts_id} }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Service={
    list:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Service/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    details:(id)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Service/details',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
};
export const Travel={
    list:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Travel/list',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Mch={
    details:(mch_id)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Mch/details',{
                data:{
                    mch_id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Login={
    wechatMPCode:(code)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Login/wechatMPCode',{ data:{code} }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    vcode:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Login/vcode',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    wechatMP:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Login/wechatMP',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getVcode:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Login/getVcode',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请后再试'});
            })
        })
    },
    wechatLogin:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Login/wechatLogin',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
};
export const Upload={
    upload:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.upload('Upload/upload',data).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    chooseImage:()=>{
        return new Promise((resolve,reject)=>{
            uni.chooseImage({
                count:1,
                success: (res) => {
                    $http.upload('Upload/upload',{
                        name: 'file',
                        filePath: res.tempFilePaths[0],
                    }).then((res)=>{
                        if(res.code===$http.ResponseCode.SUCCESS){
                            resolve(res);
                        }else{
                            reject(res);
                        }
                    }).catch(()=>{
                        reject({msg:'服务器繁忙，请稍后再试'});
                    })
                },
                fail: (err) => {
                    reject({msg:'选择图片失败'});
                }
            })
        })
    }
}
export const Coupon={
    getAvailable:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('Coupon/getAvailable').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    list(query){
        return new Promise((resolve,reject)=>{
            $http.get('Coupon/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    receiveList()
    {
        return new Promise((resolve,reject)=>{
            $http.get('Coupon/receiveList').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    receive(id){
        return new Promise((resolve,reject)=>{
            $http.get('Coupon/receive',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Orders={
    list(query){
        return new Promise((resolve,reject)=>{
            $http.get('Orders/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    create:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Orders/create',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else if(res.code===$http.ResponseCode.PAY_SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    payByOrders(data){
        return new Promise((resolve,reject)=>{
            $http.post('Orders/payByOrders',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else if(res.code===$http.ResponseCode.PAY_SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    cancel:(trade)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Orders/cancel',{
                data: {
                    trade
                }
            }).then((res)=>{
                resolve(res);
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    delete:(trade)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Orders/delete',{
                data: {
                    trade
                }
            }).then((res)=>{
                resolve(res);
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    getOrderState:(trade)=>{
        return new Promise((resolve,reject)=>{
            $http.get('Orders/getOrderState',{
                data: {
                    trade
                }
            }).then((res)=>{
                resolve(res);
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    info:(options)=>{
        let query={};
        if(typeof options==='string'){
            query.trade=options;
        }else{
            query=options;
        }
        return new Promise((resolve,reject)=>{
            $http.get('Orders/info',{
                data: query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    },
    refund:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Orders/refund',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else if(res.code===$http.ResponseCode.PAY_SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    }
}
export const AppointmentService={
    user:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('AppointmentService/user').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    technician:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('AppointmentService/technician').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    details:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('AppointmentService/details',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    verification(data){
        return new Promise((resolve,reject)=>{
            $http.post('AppointmentService/verification',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    departure(data){
        return new Promise((resolve,reject)=>{
            $http.post('AppointmentService/departure',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    arrival(data){
        return new Promise((resolve,reject)=>{
            $http.post('AppointmentService/arrival',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    startService(data){
        return new Promise((resolve,reject)=>{
            $http.post('AppointmentService/startService',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    endService(data){
        return new Promise((resolve,reject)=>{
            $http.post('AppointmentService/endService',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    getTechnicianAppointmentService(query){
        return new Promise((resolve,reject)=>{
            $http.get('AppointmentService/getTechnicianAppointmentService',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else if(res.code===$http.ResponseCode.PAY_SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    }
}
export const OrdersSub={
    list:(query)=>{
        return new Promise((resolve,reject)=>{
            $http.get('OrdersSub/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const TechnicianScheduling={
    index:()=>{
        return new Promise((resolve,reject)=>{
            $http.get('TechnicianScheduling/index').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    adjustScheduling(){
        return new Promise((resolve,reject)=>{
            $http.get('TechnicianScheduling/adjustScheduling').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    update:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('TechnicianScheduling/update',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Collect={
    service(id){
        return new Promise((resolve,reject)=>{
            $http.get('Collect/service',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    serviceList (query){
        return new Promise((resolve,reject)=>{
            $http.get('Collect/serviceList',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    technician(id){
        return new Promise((resolve,reject)=>{
            $http.get('Collect/technician',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    technicianList(query){
        return new Promise((resolve,reject)=>{
            $http.get('Collect/technicianList',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    shop(id){
        return new Promise((resolve,reject)=>{
            $http.get('Collect/shop',{
                data:{
                    id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Feedback={
    create:(data)=>{
        return new Promise((resolve,reject)=>{
            $http.post('Feedback/create',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const MoneyRegister={
    index(){
        return new Promise((resolve,reject)=>{
            $http.get('MoneyRegister/index').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    }
}
export const Wallet={
    log(query){
        return new Promise((resolve,reject)=>{
            $http.get('Wallet/log',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    technicianLog(query){
        return new Promise((resolve,reject)=>{
            $http.get('Wallet/technicianLog',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    agentLog(query){
        return new Promise((resolve,reject)=>{
            $http.get('Wallet/agentLog',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    withdrawal(data){
        return new Promise((resolve,reject)=>{
            $http.post('Wallet/withdrawal',{
                data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            });
        });
    
    }
}
export const Comment={
    create(data){
        return new Promise((resolve,reject)=>{
            $http.post('Comment/create',{
                data:data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    index(query){
        return new Promise((resolve,reject)=>{
            $http.get('Comment/index',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    reply(data){
        return new Promise((resolve,reject)=>{
            $http.post('Comment/reply',{
                data:data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    }
}
export const UserFinanceAccount={
    index(){
        return new Promise((resolve,reject)=>{
            $http.get('UserFinanceAccount/index').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    },
    save(data){
        return new Promise((resolve,reject)=>{
            $http.post('UserFinanceAccount/save',{
                data:data
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    },
}
export const Agent={
    info(){
        return new Promise((resolve,reject)=>{
            $http.get('Agent/info').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    },
    apply(data){
        return new Promise((resolve,reject)=>{
            $http.post('Agent/apply',{ data }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res);
                }else{
                    reject(res);
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'});
            })
        })
    },
    user(){
        return new Promise((resolve,reject)=>{
            $http.get('Agent/user').then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    },
    statistics(query){
        return new Promise((resolve,reject)=>{
            $http.get('Agent/statistics',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    }
}
export const Shop={
    list(query){
        return new Promise((resolve,reject)=>{
            $http.get('Shop/list',{
                data:query
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    },
    details(shop_id){
        return new Promise((resolve,reject)=>{
            $http.get('Shop/details',{
                data:{
                    shop_id
                }
            }).then((res)=>{
                if(res.code===$http.ResponseCode.SUCCESS){
                    resolve(res.data);
                }else{
                    reject(res)
                }
            }).catch(()=>{
                reject({msg:'服务器繁忙，请稍后再试'})
            })
        })
    }
}