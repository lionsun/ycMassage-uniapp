import store from '@/store'
export const $auth={
	auth:(options)=>{
		switch(options.action){
			case 'user':
				return store.getters.hasUserPermission(options.permission);
			default:
				return store.getters.hasPermission(options.permission);
		}
	},
	user:(permission)=>{
		return store.getters.hasUserPermission(permission);
	}
}   