import App from './App'

import cuCustom from '@/colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

import globalMixin from '@/utils/globalMixin';
Vue.mixin(globalMixin);

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
import store from './store'
Vue.prototype.$store = store

import * as utils from "./utils/index.js"
Vue.prototype.$utils=utils;

App.mpType = 'app'
const app = new Vue({
  store,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif